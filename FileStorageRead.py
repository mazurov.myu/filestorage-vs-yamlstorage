import os
import sys
import cv2
import time 

if __name__ == "__main__":
	if len(sys.argv) < 2:
		raise Exception("You should tell what file to read")
	file = sys.argv[1]

	if not(os.path.exists(file)):
		raise FileNotFoundError(f"file {file} not found")

	start_opening_time = time.time()
	fs = cv2.FileStorage(file, cv2.FileStorage_READ)
	finish_opening_time = time.time()

	start_reading_time = time.time()
	num_shots = 0

	shotsNode = fs.getNode("shots")

	for num_shot in range(shotsNode.size()):

		num_shots += 1

		shotNode = shotsNode.at(num_shot)

		shotNode

		grabNum = shotNode.getNode("grabNum").real()
		grabTime = shotNode.getNode("grabTime").real()

		dataNode = shotNode.getNode("data")

		if dataNode.isSeq():
			pass
		if dataNode.isMap():
			for dataKey in dataNode.keys():
				sensorNode = dataNode.getNode(dataKey)
				for sensor_attr_name in sensorNode.keys():
					sensor_attrNode = sensorNode.getNode(sensor_attr_name)
					print(sensor_attr_name ,sensor_attrNode.real())


			for datakey in dataNode.keys():
				sensorNode = dataNode.getNode(datakey)
				for sensor_attr_name in sensorNode.keys():
					sensor_attr_node = sensorNode.getNode(sensor_attr_name)
					if sensor_attr_node.isReal():
						print(sensor_attr_name, sensor_attr_node.real())
					if sensor_attr_node.isMap():
						for sub_node_name in sensor_attr_node.keys():
							sub_node = sensor_attr_node.getNode(sub_node_name)
							if sensor_attr_node.isReal():
								print("\t", sensor_attr_name, sensor_attr_node.real())
	
	finish_reading_time = time.time()

	fs.release()

	print(f"opening file time is: {finish_opening_time - start_opening_time} s\nreading through whole file lasted: {finish_reading_time - start_reading_time} s\nand processed {num_shots} shots of data measurements")





