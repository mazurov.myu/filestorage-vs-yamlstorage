# FileStorage vs YamlStorage

Репозиторий для сравнения обработчиков последовательных логов проездов. 
Сравниваются Yaml файл, созданный выкачиванием вложенного словаря с помощью pyyaml и Yaml, созданный с помощью OpencvFileStorage.

Чтобы сконвертировать данные с логов в новый формат, пригодный для открытия в opencv необходимо:
python3 convertor_old2opencv.py [path]
Например:

python3 convertor_old2opencv.py example_data/data_[tab]


Чтобы запустить считывание данных из старого yaml
python3 YamlBaseReader.py example_data/старый файл

Чтобы запустить считывание из opencv файла:
python3 FileStorageRead.py example_data/opencv[tab]



Сравнение. 
- Filestorage READING
opening file time is: 0.07473015785217285 s
reading through whole file lasted: 1.5705087184906006 s
and processed 7533 shots of data measurements

- Base Yaml READING
opening file time is: 46.99978303909302 s
reading through whole file lasted: 1.0805082321166992 s
and processed 7533 shots of data measurements
