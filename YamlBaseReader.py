import os
import sys
import yaml
import time 

if __name__ == "__main__":
	if len(sys.argv) < 2:
		raise Exception("You should tell what file to read")
	file = sys.argv[1]

	if not(os.path.exists(file)):
		raise FileNotFoundError(f"file {file} not found")

	start_opening_time = time.time()
	with open(file, "r") as f:
		data = yaml.safe_load(f)
	finish_opening_time = time.time()

	start_reading_time = time.time()
	num_shots = 0

	for num, it in data.items():
		num_shots += 1

		grabNum = it["grabNum"]
		grabTime = it["grabTime"]

		data = it["data"]

		for sensor_data in data:
			for sensor_attr in sensor_data.keys():
				sensor_attr_data = sensor_data[sensor_attr]

				for sensor_supattr_name in sensor_attr_data.keys():
					sensor_supattr_value = sensor_attr_data[sensor_supattr_name]
					if isinstance(sensor_supattr_value, dict):
						for s_name, s_value in sensor_supattr_value.items():
							print(s_name, s_value)
					else:
						print(sensor_supattr_name, sensor_supattr_value)


			# for sensor_attr in sensor_data:
				# print(sensor_attr, type(sensor_attr))
	
	finish_reading_time = time.time()

	print(f"opening file time is: {finish_opening_time - start_opening_time} s\nreading through whole file lasted: {finish_reading_time - start_reading_time} s\nand processed {num_shots} shots of data measurements")
