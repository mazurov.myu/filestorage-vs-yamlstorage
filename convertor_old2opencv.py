#!/usr/bin/python3

import os
import cv2
import sys
import yaml


if __name__ == "__main__":


	# print(sys.argv)
	if len(sys.argv) < 2:
		raise Exception("You should teel what file to convert")
	file = sys.argv[1]

	if not(os.path.exists(file)):
		raise FileNotFoundError(f"file {file} not found")

	idx = file.rfind("/")
	if idx == -1:
		opencv_file_config = "opencv2_" + file
	else:
		opencv_file_config = file[:idx+1] + "opencv2_" + file[idx+1:]

	print(f"будет преобразовано в {opencv_file_config}")

	with open(file, "r") as f:
		data = yaml.safe_load(f)

	fs = cv2.FileStorage(opencv_file_config, cv2.FileStorage_WRITE)

	fs.startWriteStruct("shots", cv2.FileNode_SEQ)


	for key, shot_dict in data.items():
		data_dict = shot_dict["data"]
		grabNum = shot_dict["grabNum"]
		grabTime = shot_dict["grabTime"]

		fs.startWriteStruct("", cv2.FileNode_MAP)

		fs.write("grabNum", grabNum)
		fs.write("grabTime", grabTime)
		fs.startWriteStruct("data", cv2.FileNode_MAP)

		for sense_dict in data_dict:

			sensor_name = list(sense_dict.keys())[0]
			sensor_data = sense_dict[sensor_name]

			fs.startWriteStruct(sensor_name, cv2.FileNode_MAP)
			for sensor_attr, sensor_value in sensor_data.items():
				if isinstance(sensor_value, dict):
					fs.startWriteStruct(sensor_attr, cv2.FileNode_MAP)
					for sub_name, sub_value in sensor_value.items():
						sub_name = sub_name.replace("[", "")
						sub_name = sub_name.replace("]", "")
						fs.write(sub_name, sub_value)
					fs.endWriteStruct()
				else:
					fs.write(sensor_attr, sensor_value)

			fs.endWriteStruct()


		fs.endWriteStruct()

		fs.endWriteStruct()

	fs.endWriteStruct()

	fs.release()




